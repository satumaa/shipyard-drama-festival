import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import iconQuestion from '../assets/images/icon-question.svg';

const Form = () => {

    // State for form completion
    const [complete, setComplete] = useState('');

    // React hook form definitions
    const { register, handleSubmit, formState: { errors }, reset} = useForm();

    // Handle form submit
    const onSubmit = data => {

        // Set state to completed
        setComplete('form-completed');

        // Output values to console
        console.table({
            "Etunimi": data.firstname,
            "Sukunimi": data.lastname,
            "Ruokavalio": data.diet,
            "Juomavalinta": data.drink,
            "Kyyti": data.pickup
        });

        // Output values to post table cells
        const postTableValues = document.querySelectorAll(".tr-value");
        postTableValues[0].textContent = data.firstname;
        postTableValues[1].textContent = data.lastname;
        postTableValues[2].textContent = data.diet ? data.diet : "-";
        postTableValues[3].textContent = data.drink;
        postTableValues[4].textContent = data.pickup ? "Tarvitsen kyydin" : "En tarvitse kyytiä";

        // Finally reset the form
        reset();
    }


    useEffect( () => {
        // Get all fields in the form
        const inputs = document.querySelectorAll(".form-field");

        // Add "has-value" -class if field has any input content
        inputs.forEach( (elem) => {
            elem.addEventListener("change", (e) => {
                if (e.target.value) {
                    e.target.classList.add("has-value");
                } else {
                    e.target.classList.remove("has-value");
                }
            })
        })
    }, [])

    return (

        <div id="signup-form" className={complete}>

            <form onSubmit={handleSubmit(onSubmit)}>

                <h2>Ilmoittautumislomake (testiversio)</h2>

                <div className={errors.firstname ? "form-group has-error" : "form-group"}>
                    <input 
                        type="text"
                        id="firstname"
                        className="form-field"
                        {...register('firstname', { required: true })}
                    />
                    <label htmlFor="firstname" className="control-label">Etunimi *</label><i className="focus-bar"></i>

                    <div className="error-message-area">
                        {errors.firstname && <span>Täytä etunimi</span>}
                    </div>
                </div>


                <div className={errors.lastname ? "form-group has-error" : "form-group"}>
                    <input 
                        type="text"
                        id="lastname"
                        className="form-field"
                        {...register('lastname', { required: true })}
                    />
                    <label htmlFor="lastname" className="control-label">Sukunimi *</label><i className="focus-bar"></i>

                    <div className="error-message-area">
                        {errors.lastname && <span>Täytä sukunimi</span>}
                    </div>
                </div>


                <div className="form-group">
                    <textarea 
                        id="diet"
                        className="form-field"
                        {...register('diet', { required: false })}
                    >
                    </textarea>
                    <label htmlFor="diet" className="control-label">Ruokavalio</label><i className="focus-bar"></i>
                </div>


                <div className="form-group">
                    <select {...register('drink', { required: false })}>
                        <option>Valkoviini</option>
                        <option>Punaviini</option>
                        <option>Olut</option>
                        <option>Alkoholiton</option>
                    </select>
                    <label htmlFor="select" className="control-label">Juomavalinta</label><i className="focus-bar"></i>
                </div>


                <div className="form-group">
                    <div className="checkbox">
                        <label>
                            <input type="checkbox" {...register('pickup', { required: false })}/>
                            <i className="helper"></i>Tarvitsen kyydin
                        </label>
                        <div className="checkbox-info">
                            <span><img src={iconQuestion} alt="More info" /></span>
                            <span className="info-content">
                                Jos et pääse paikalle omin avuin, tarjoamme tarvittaessa kyydin juhlapaikalle. Ilmoittautuneille annetaan lisäinfoa viimeistään viikko ennen juhlatilaisuutta.
                            </span>
                        </div>
                    </div>
                </div>


                <div className="button-container">
                    <input type="submit" value="Lähetä" className="submit-button" />
                </div>


            </form>


            <div className="form-post-message">
                <h2>Kiitos ilmoittautumisesta!</h2>
                <hr />
                <p>Lomake lähetetty seuraavilla tiedoilla:</p>

                <table id="form-post-table">
                    <tbody>
                        <tr>
                            <td className="tr-name">Etunimi</td>
                            <td className="tr-value"></td>
                        </tr>
                        <tr>
                            <td className="tr-name">Sukunimi</td>
                            <td className="tr-value"></td>
                        </tr>
                        <tr>
                            <td className="tr-name">Ruokavalio</td>
                            <td className="tr-value"></td>
                        </tr>
                        <tr>
                            <td className="tr-name">Juomavalinta</td>
                            <td className="tr-value"></td>
                        </tr>
                        <tr>
                            <td className="tr-name">Kyytitarve</td>
                            <td className="tr-value"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    )
}

export default Form;