import React, {useEffect} from 'react';
import Form from '../components/Form';

function Info() {

    useEffect(() => {
        // Set the page title
        document.title = "Info | Shipyard Drama Festival";
    }, []);

    return (
        <main className="main-content" id="main-content-telakka">
            <h2>Ruissalon Telakka</h2>
            <p>27.8. klo XX–myöhään</p>

            <p>Ruisrock ja Flow jäävät tältä kesältä väliin, mutta ei hätää. Päätimme ilahduttaa sinua ja muita läheisimpiä tyyppejämme privaattifestareilla.</p>

            <p>Tee siis jo nyt perjantai-illaksi 27.8. kalenteribuukkaus ”Shipyard Drama Festival”, merkitse paikaksi Ruissalon Telakka ja ala jo hiljalleen varoitella kotiväkeä, että tuolloin saattaa mennä vähän myöhempään.</p>

            <p>Ihan hetkessä ei selvitä, sillä festarit ovat samalla Drama Queenin 25-vuotisbileet ja pääesiintyjänä toimii suomiräpin jättiläinen ja baarin seksikkäin jäbä, Pyhimys.</p>

            <p>Ilmoittautumislinkkejä, festaripasseja ja muuta lisätietoa luvassa hieman myöhemmin, stay tuned.</p>

            <p>Festariterveisin</p>
            <p>Drama Queenit</p>

            <p>shipyardfestival.fi</p>
            <p>P.S. Kekkerit järjestetään tietenkin vain, mikäli koronatilanne sen sallii. Pidetään peukkuja.</p>

            <Form />
        </main>
    )
}

export default Info;