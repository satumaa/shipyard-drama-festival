import React, {useEffect} from 'react';
import Logo25 from '../assets/images/logo-25years.svg';

function Home() {

    useEffect(() => {
        // Set the page title
        document.title = "Shipyard Drama Festival";
    }, []);

    return (
        <main className="main-content" id="main-content-home">

            <div className="home-bg"></div>

            <div className="marquee-bg">
                <div className="marquee-row"></div>
                <div className="marquee-row"></div>
                <div className="marquee-row"></div>
                <div className="marquee-row"></div>
            </div>

            <div className="event-date">
                <time dateTime="27.8.2021">27.8.2021</time>
                <a href='webcal://assets/calendar/shipyardfestival.ics' title="Tallenna tapahtuma kalenteriin ICS-tiedostona" download>Tallenna kalenteriin</a>
            </div>

            <img id="logo-25years" src={Logo25} alt="25 years of DQ" />

        </main>
    )
}

export default Home;