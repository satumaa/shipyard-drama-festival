import React, {useEffect} from 'react';


function Artists() {

    useEffect(() => {
        // Set the page title
        document.title = "Artistit | Shipyard Drama Festival";
    }, []);

    return (
        <main className="main-content" id="main-content-history">

            <h2>25 Years of Drama</h2>
            <p>Drama Queenin 25 vuoteen on mahtunut yhtä sun toista. Loistotyyppejä – asiakkaita, deequja, toimistokoiria. Mainontaa, viestintää, digiä. Turkua, Helsinkiä, Tukholmaa. Vähän Cannesiakin. Kasvua perustajakolmikosta XX ammattilaiseen. Huonoja vitsejä. Hyviä bileitä.</p>

            <p>Kaikkea tätä on luvassa myös 27.8. Nähdään Shipyard Festivaleilla.</p>

        </main>
    )
}

export default Artists;