import React, { useEffect } from 'react'
import logo from './assets/images/logo-dqc.svg';
import './App.scss';
import { BrowserRouter as Router, Switch, Route, NavLink } from "react-router-dom";
import Home from './pages/Home';
import History from './pages/History';
import Pyhimys from './pages/Pyhimys';
import Telakka from './pages/Telakka';


function App() {

	useEffect( () => {

		// Define nav elements
		const mainNav = document.querySelector("#main-nav");
		const menuOpen = document.querySelector("#nav-open");
		const menuClose = document.querySelector("#nav-close");
		const navWrapper = document.querySelector("#nav-wrapper");
		const navLinks = document.querySelectorAll(".nav-link");
		const dqLogo = document.querySelector("#dq-logo");


		// Collapse nav to mobile view if window < 768px by toggling nav class
		const checkWindowSize = () => {
			if (window.innerWidth <= 768) {
				mainNav.classList.add("mobile-view")
			} else {
				mainNav.classList.remove("mobile-view")
			}
		}

		// Run window width check on page load & window resize
		checkWindowSize();
		window.addEventListener('resize', checkWindowSize);



		// Activate/deactivate mobile menu
		const navActivation = (navState) => {
			if (navState) {
				navWrapper.classList.add('active');
				document.documentElement.classList.add('nav-open');
			} else {
				navWrapper.classList.remove('active');
				document.documentElement.classList.remove('nav-open');
			}
		}

		// Toggle active mobile-menu with open/close buttons
		menuOpen.addEventListener('click', () => {
			navActivation(true);
		});
		menuClose.addEventListener('click', () => {
			navActivation(false);
		});

		// Remove active mobile when any nav link clicked
		navLinks.forEach( (link) => {
			link.addEventListener('click', () => {
				navActivation(false);
			})
		})

		// Remove active mobile when logo clicked
		dqLogo.addEventListener('click', () => {
			navActivation(false);
		});


	}, [])
	
  	return (
		<div className="App">

			{/* Fix url for deployment app
			https://stackoverflow.com/questions/47601290/react-router-url-issues-after-deployment */}
			<Router basename={`${process.env.PUBLIC_URL}`}>
				<header className="site-header">
					<div className="header-content">
					
						<h1>DQ25</h1>
						<NavLink to={`/`} id="dq-logo">
							<img src={logo} alt="DQ" />
						</NavLink>

						<nav id="main-nav">

							<button id="nav-open" aria-label="Open menu"></button>

							<div id="nav-wrapper">

								<button id="nav-close" aria-label="Close menu"></button>

								<ul>
									<li className="nav-calendar-link">
										<a href='webcal://../asssets/calendar/shipyardfestival.ics' title="Tallenna tapahtuma kalenteriin ICS-tiedostona" download>Tallenna kalenteriin</a>
									</li>
									<li>
										<NavLink to={`/25-years-of-drama`} className="nav-link" activeClassName="active">25 Years of Drama</NavLink>
									</li>
									<li>
										<NavLink to={`/pyhimys`} className="nav-link" activeClassName="active">Pyhimys</NavLink>
									</li>
									<li>
										<NavLink to={`/ruissalon-telakka`} className="nav-link" activeClassName="active">Ruissalon Telakka</NavLink>
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</header>



				<Switch>
					<Route path={`/25-years-of-drama`}>
						<History />
					</Route>
					<Route path={`/pyhimys`}>
						<Pyhimys />
					</Route>
					<Route path={`/ruissalon-telakka`}>
						<Telakka />
					</Route>
					<Route path={`/`}>
						<Home />
					</Route>
				</Switch>

					
			</Router>


    	</div>
  	);
}

export default App;
